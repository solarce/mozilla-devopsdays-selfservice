!SLIDE

# Productivity via
# Self Service

![lolcat1](productive.jpg)

!SLIDE

# Who Am I?

## @solarce
## lolcat enthusiast

![me](wizard.jpg)


.notes * Brandon Burton * Web Operations Engineer * [@solarce](http://www.twitter.com/solarce)

!SLIDE

# Mozilla?

![mozilla](mozilla.jpg)

!SLIDE

# Firefox 

Firefox - great browser or greatest browser?

!SLIDE

# Open Web

![openweb](open_web_award.png)

.notes we believe in an open web, which means we run a lot of web sites
and web services

