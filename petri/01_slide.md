# What's next?


.notes next
* Continuous Delivery
* Internal IaaS/PaaS

!SLIDE

# C to the D

![cd](cd.jpg)

.notes cd
* dev deployed from github webhook, freddo
* stage and prod with one-button deploys, chief


!SLIDE

# IaaS/PaaS

![cloudboss](cloudboss.jpg)

!SLIDE

# Petri

[Project Petri](https://wiki.mozilla.org/Petri)

* Project Petri is a 1-year experiment in providing a
* Platform-as-a-service (PaaS) and/or Infrastructure-as-a-Service (IaaS)
offering

!SLIDE

# IaaS Options

* OpenStack
* Eucalyptus
* OpenNebula
* Ganeti+KVM+Scripts

!SLIDE

# PaaS Options

* CloudFoundry
* Stackato
* RedHat OpenShift

!SLIDE

# Version 1.0
## Eucalyptus 3.0
## Cloud Foundry

.notes v1 decision
* Euc had best EC2 (AWS) tools compatibility, re-use AMIs/tools?
* CF was only viable and open source solution at the time/this time

!SLIDE

# CF now

![cf](cf.jpg)

.notes cfnow
* Alpha CF service being used
* Q3 goal to provide CF with SLA
* WebOps interest

!SLIDE
 
# CF: Awesome

![awesome](awesome.jpg)

.notes awesome
* easy for devs to use
* we wrote an ldap proxy
* cf services idea is very cool

!SLIDE

# CF: Challenge

![challenge](challenge.jpg)

.notes challenges
* dependencies
* pushing all your stuff
** 200-300MB checkouts :~(
* security model
* vmware releasing code is ~

!SLIDE

#Eucalyptus

![euc](euc.jpg)

.notes euc
* still getting hardware

!SLIDE

# In Closing

!SLIDE

![thereisnoneed](thereisnoneed.jpg)

!SLIDE

# slides

slides up on
 
[github.com/solarce/mozilla-selfservice](https://github.com/solarce/mozilla-selfservice)

[bburtonmozillaselfops.herokuapp.com](http://bburtonmozillaselfops.herokuapp.com)
