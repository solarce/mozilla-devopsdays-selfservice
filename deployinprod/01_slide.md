!SLIDE

# 

# In Prod

![prod](prod.jpg)

!SLIDE

# Infrastructure

![infra](infra.jpg)

.notes infra
* dev/stage/prod
* everything load balanced
* services are built in clusters

!SLIDE

# Clusters

![cluster](cluster.jpg)

.notes clusters
 * generic clusters for smaller apps
 * dedicated clusters for some larger apps
 * admin node

!SLIDE

# Admin node

.notes admin node
 * where code updates are pulled to
 * where deploys are done from

!SLIDE

# Architecture

![arch](web_cluster_physical_layout.png)

!SLIDE

# deploying
![deploy](new_web_cluster_push_flow.png)

.notes deploy
* app code is in ```/data/{dev|stage|prod}/src/site/code```
* since most our apps are open source, certain settings file aren't in
  public repos
* update script is run
* update script calls deploy script (commander wrapper)

!SLIDE

# Upsides

![approve](approval.jpg)

.notes update
* IT managed and supported
* Secured and monitored by OpSec
* Stable and Fast (so far)

!SLIDE

# Downsides

![disapprove](disapprove.jpg)

.notes downside
* Limit to langs/libss/etc supported
* Pushes involve IT
* If it a lot of pushes at once, get in line

