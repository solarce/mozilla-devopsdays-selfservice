!SLIDE

# About IT

![it](it.jpg)

.notes * Mozilla IT is organized into a number of groups and teams

!SLIDE

# Groups

![groups](group.jpg)

.notes dem groups 
* Systems 
* Site Reliability (SREs) * Desktop * NetOps * DCOps * Etc
* a lot of the teams list above have been formalized as of the beginning of this year. Mozilla has seen tremendous growth in the user base and so also in the company in the last two years and so the formalizing of Mozilla IT into more distinct teams is a new, but natural outgrowth of the userbase and employee growth.

!SLIDE

# Teams

![team](team.jpg)

.notes teams
* WebOps
 * Databases
 * Storage/Virtulization
 * Infrastructure
 * Developer Services
 * Systems Tools
 * etc
 
!SLIDE

# How we work

![work](work.jpg)

.notes work
* SRE team is oncall
* A lot of skill crossover 
* Focus on specific responsibilities
* Knowledge of shared services/processes
* Systems has a shared responsibility for service levels

!SLIDE

# Some history

.notes what geek doesn't love a good car analogy

!SLIDE

# 3 years ago 

## Mostly PHP

![php](php.jpg)


.notes three
* Mostly PHP and MySQL
* Very adhoc

!SLIDE

# Two years ago 

## Moving to Python

![two](some_python.jpg)


.notes two
* Mix of PHP/MySQL and Django/MySQL
* Structure evolving
* Start of Puppetizing

!SLIDE

# One year ago 

## Lots of Python
## Clusters, Puppet

![one](yearago.jpg)

.notes one
* Mostly Django/MySQL
* Well defined structure and deployment workflow
* Lots of Puppet and Python Automation

!SLIDE

# Today ...

## Django, Puppet, Webops

![today](today.jpg)

.notes today
* IT has grown
* Webops was two people a year ago, today is 8
* Rest of IT has grown accordingly

!SLIDE

# Growth!

![growth](growth.jpg)

.notes but
* Mozilla has grown too
* User base has grown
* Demand for sites and resources always in the lead
 
